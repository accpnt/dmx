library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.all;

entity dmx512_test is
end dmx512_test;

architecture dmx512_test_arch of dmx512_test is
	-- constants

	constant C_CLKS_PER_BIT : integer := 40;
	constant C_BIT_PERIOD : time := C_CLKS_PER_BIT * 100 ns;

	-- components
	component dmx_rx is
	generic (
		G_CLKS_PER_BIT : natural := 40     -- needs to be set correctly
	);
	port(
		clk        : in  std_logic;
		rst        : in  std_logic;  -- asynchronous reset
		rx         : in  std_logic;
		start      : in  std_logic;
		data       : out std_logic_vector(7 downto 0);
		ready      : out std_logic
	);
	end component dmx_rx;

	component dmx512_tx is
	generic (
		G_CLKS_PER_BIT : natural := 40  -- divisor needs to be set correctly
	);
	port(
		clk   : in  std_logic;
		rst   : in  std_logic;  -- asynchronous reset
		load  : in  std_logic;
		data  : in  std_logic_vector(7 downto 0);
		tx    : out std_logic;  -- connected to DMX512 bus
		ready : out std_logic   -- equals '1' if frame transmission is over
	);
	end component dmx512_tx;

	component dmx512_rx is
	generic (
		G_CLKS_PER_BIT : natural := 40     -- needs to be set correctly
	);
	port(
		clk        : in  std_logic;
		rst        : in  std_logic;  -- asynchronous reset
		rx         : in  std_logic;
		start      : in  std_logic;
		data       : out std_logic_vector(7 downto 0);
		ready      : out std_logic
	);
	end component dmx512_rx;

	-- signals
	signal s_clk : std_logic := '0';
	signal s_rst : std_logic := '0';
	signal s_tx_load : std_logic := '0';
	signal s_tx_ready : std_logic := '0';
	signal s_tx : std_logic := '0';
	signal s_tx_data : std_logic_vector(7 downto 0) := (others => '0');
	signal s_rx_data : std_logic_vector(7 downto 0) := (others => '0');
	signal s_rx : std_logic := '1';
	signal s_rx_ready : std_logic := '0';

	-- low-level byte-write
	procedure dmx512_write_byte (
		byte : in  std_logic_vector(7 downto 0)) is
	begin
		--s_tx_data <= byte;
	end dmx512_write_byte;

begin
	-- instantiate DMX512 transmitter
	dmx512_tx_object : dmx512_tx
	generic map (
		G_CLKS_PER_BIT => C_CLKS_PER_BIT
	)
	port map (
		clk   => s_clk,
		rst   => s_rst,
		load  => s_tx_load,
		data  => s_tx_data,
		tx    => s_tx,
		ready => s_tx_ready
	);

	-- instantiate DMX512 receiver
	dmx512_rx_object : dmx512_rx
	generic map (
		G_CLKS_PER_BIT => C_CLKS_PER_BIT
	)
	port map (
		clk    => s_clk,
		rst    => s_rst,
		rx     => s_tx,
		start  => s_tx_ready,
		data   => s_rx_data,
		ready  => s_rx_ready
	);

	rst_process :process
	begin
		s_rst <= '0';
		wait for 100 ns;
		s_rst <= '1';
		wait;
	end process rst_process;

	clk_process : process
	begin
		s_clk <= '0';
		wait for 50 ns;
		s_clk <= '1';
		wait for 50 ns;
	end process clk_process;

	sim_process : process
	begin
		wait for 200 ns;  -- wait enough time for reset to rise
		s_tx_load <= '1';

		-- Tell the UART to send a command.
		wait until rising_edge(s_clk);
		-- dmx512_write_byte(X"FF");
		s_tx_data <= X"FF";

		-- Check that the correct command was received
		--if w_RX_BYTE = X"3F" then
		--	report "Test Passed - Correct Byte Received" severity note;
		--else
		--	report "Test Failed - Incorrect Byte Received" severity note;
		--end if;

		--assert false report "Tests Complete" severity failure;

	end process;

end dmx512_test_arch;
