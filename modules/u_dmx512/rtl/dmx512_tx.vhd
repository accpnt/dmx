-- This file contains the DMX512 transmitter. This receiver is able to
-- receive 8 bits of serial data, one start bit, one stop bit,
-- and no parity bit.  When receive is complete o_rx_dv will be
-- driven high for one clock cycle.
--
-- Set Generic G_CLKS_PER_BIT as follows:
-- G_CLKS_PER_BIT = (frequency of clk)/(frequency of DMX512 bus)
-- Example: 10 MHz Clock : (10000000)/(250000) = 40

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity dmx512_tx is
	generic (
		G_CLKS_PER_BIT : natural := 40     -- divisor needs to be set correctly
	);
	port(
		clk   : in  std_logic;
		rst   : in  std_logic;  -- asynchronous reset
		load  : in  std_logic;
		data  : in  std_logic_vector(7 downto 0);
		tx    : out std_logic;  -- connected to DMX512 bus
		ready : out std_logic   -- equals '1' if frame transmission is over
	);
end entity dmx512_tx;

architecture dmx512_tx_arch of dmx512_tx is
	-- components

	-- constants

	-- types
	type state_type is (MTBP, BREAK, MAB1, MAB2, START, D0, D1, D2, D3, D4, D5, D6, D7, STOP1, STOP2);

	-- signals
	signal s_rx        : std_logic := '0';
	signal s_clk_count : natural range 0 to G_CLKS_PER_BIT-1 := 0;
	signal state       : state_type := MTBP;
begin

	-- combinatorial

	dmx512_tx_process : process(clk, rst)
	begin
		if rst = '0' then
			state <= MTBP;
			ready <= '1';
			tx <= '0';
		elsif rising_edge(clk) then
			if s_clk_count < G_CLKS_PER_BIT - 1 then
				case state is
				when MTBP =>
					ready <= '1';
					tx    <= '1';
					if load = '1' then
						state <= BREAK;
					end if;
				when BREAK =>
					tx <= '0';
					state <= MAB1;
				when MAB1 =>
					tx <= '1';
					state <= MAB2;
				when MAB2 =>
					tx <= '1';
					state <= START;
				when START =>
					tx <= '0';
					state <= D0;
				when D0 =>
					tx <= data(7);
					state <= D1;
				when D1 =>
					tx <= data(6);
					state <= D2;
				when D2 =>
					tx <= data(5);
					state <= D3;
				when D3 =>
					tx <= data(4);
					state <= D4;
				when D4 =>
					tx <= data(3);
					state <= D5;
				when D5 =>
					tx <= data(2);
					state <= D6;
				when D6 =>
					tx <= data(1);
					state <= D7;
				when D7 =>
					tx <= data(0);
					state <= STOP1;
				when STOP1 =>
					tx <= '1';
					state <= STOP2;
				when STOP2 =>
					tx <= '1';
					state <= BREAK;
				when others =>
					state <= MTBP;
				end case;
			else
				s_clk_count <= s_clk_count + 1;
			end if;
		end if;
	end process dmx512_tx_process;
end dmx512_tx_arch;
