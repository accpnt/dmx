-- This file contains the DMX512 receiver. This receiver is able to
-- receive 8 bits of serial data, one start bit, one stop bit,
-- and no parity bit.  When receive is complete o_rx_dv will be
-- driven high for one clock cycle.
--
-- Set Generic G_CLKS_PER_BIT as follows:
-- G_CLKS_PER_BIT = (frequency of clk)/(frequency of DMX512 bus)
-- Example: 10 MHz Clock : (10000000)/(250000) = 40

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity dmx512_rx is
	generic (
		G_CLKS_PER_BIT : natural := 40     -- needs to be set correctly
	);
	port(
		clk        : in  std_logic;
		rst        : in  std_logic;  -- asynchronous reset
		rx         : in  std_logic;
		start      : in  std_logic;
		data       : out std_logic_vector(7 downto 0);
		ready      : out std_logic
	);
end entity dmx512_rx;

-- iterative
architecture dmx512_rx_arch of dmx512_rx is
	-- components

	-- constants

	-- types
	type state_type is (WAIT_BREAK, WAIT_MAB, WAIT_START, SAMPLE_START, SAMPLE0, SAMPLE1, SAMPLE2, SAMPLE3, SAMPLE4, SAMPLE5, SAMPLE6, SAMPLE7, SAMPLE_STOP1, SAMPLE_STOP2);

	-- signals
	signal s_rx        : std_logic := '0';
	signal s_rx0       : std_logic := '0';
	signal s_clk_count : natural range 0 to G_CLKS_PER_BIT-1 := 0;
	signal state       : state_type := WAIT_BREAK;
begin

	-- combinatorial

	-- purpose: double-register the incoming data.
	-- this allows it to be used in the DMX512 rx clock domain.
	-- (it removes problems caused by metastabiliy)
	dmx512_sample_process : process(clk, rst)
	begin
		if rst = '0' then
			ready <= '1';
		elsif rising_edge(clk) then
			s_rx0      <= rx;
			s_rx <= s_rx0;
		end if;
	end process dmx512_sample_process;

	dmx512_count_slots_process : process(clk, rst)
	begin
		if rst = '0' then
			ready <= '1';
		elsif rising_edge(clk) then
		end if;
	end process dmx512_count_slots_process;

	dmx512_rx_process : process(clk, rst)
	begin
		if rst = '0' then
			ready <= '1';
		elsif rising_edge(clk) then
			if s_clk_count < G_CLKS_PER_BIT - 1 then
				case state is
				when WAIT_BREAK =>
					if s_rx = '1' then
						state <= WAIT_BREAK;
					else
						state <= WAIT_MAB;
					end if;
				when WAIT_MAB =>
					if s_rx = '0' then
						state <= WAIT_START;
					end if;
				when WAIT_START =>
					if s_rx = '0' then
						state <= SAMPLE_START;
					else
						state <= WAIT_START;
					end if;
				when SAMPLE_START =>
					state <= SAMPLE0;
				when SAMPLE0 =>
					data(0) <= s_rx;
					state <= SAMPLE1;
				when SAMPLE1 =>
					data(1) <= s_rx;
					state <= SAMPLE2;
				when SAMPLE2 =>
					data(2) <= s_rx;
					state <= SAMPLE3;
				when SAMPLE3 =>
					data(3) <= s_rx;
					state <= SAMPLE4;
				when SAMPLE4 =>
					data(4) <= s_rx;
					state <= SAMPLE5;
				when SAMPLE5 =>
					data(5) <= s_rx;
					state <= SAMPLE6;
				when SAMPLE6 =>
					data(6) <= s_rx;
					state <= SAMPLE7;
				when SAMPLE7 =>
					data(7) <= s_rx;
					state <= SAMPLE_STOP1;
				when SAMPLE_STOP1 =>
					state <= SAMPLE_STOP2;
				when SAMPLE_STOP2 =>
					state <= WAIT_BREAK;
				when others =>
				end case;
			else
				s_clk_count <= s_clk_count + 1;
			end if;
		end if;
	end process dmx512_rx_process;
end dmx512_rx_arch;
