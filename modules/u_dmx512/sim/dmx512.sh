#!/bin/sh

# create library
vlib work
vmap work work

# compile RTL files
vcom -93 -work work ../u_dmx512/rtl/dmx512_rx.vhd
vcom -93 -work work ../u_dmx512/rtl/dmx512_tx.vhd

# compile testbench
vcom -93 -work work ../tst/dmx512_test.vhd

# launch ModelSim 
vsim -t 1ps dmx512_test -do ./dmx512.do
