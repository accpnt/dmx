onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /dmx512_test/s_clk
add wave -noupdate /dmx512_test/s_rst
add wave -noupdate /dmx512_test/s_tx_load
add wave -noupdate /dmx512_test/s_tx_ready
add wave -noupdate /dmx512_test/s_tx
add wave -noupdate /dmx512_test/s_tx_data
add wave -noupdate /dmx512_test/s_rx_data
add wave -noupdate /dmx512_test/s_rx
add wave -noupdate /dmx512_test/s_rx_ready
add wave -noupdate /dmx512_test/C_CLKS_PER_BIT
add wave -noupdate /dmx512_test/C_BIT_PERIOD
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {19278294 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 173
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {928663862046 ps} {928677533577 ps}
