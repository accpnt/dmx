# dmx

## Summary

This design allows DMX512 slot value extraction based on address. 

## Addressing

There is no direct addressing of the lighting peripherals on the DMX512 protocol. Each packet contains up to 512 slots of data for each destination devices. First slot addresses the first address, and so on until the last slot (maximum 512) is assigned. 

The DMX stream always starts with slot 1 after the DMX start code. When there are less then 512 slots transmitted, then the receiver must do some timing logic in order to know how many slots are transmitted. 

Slots are always sent consecutively, from 1 to 512. There is no addressing in the frame.

If you only send 123 slots, the 123rd slot is the last one in the frame.

If you only receive 172 slots,  you get all the data for channels 1 through 172 in that order. Same for 512 etc.

## Protocol

DMX512 data is transmitted sequentially in asynchronous serial format, starting with slot 0 and ending with slot 512. Prior to the first data slot being transmitted, there is a reset sequence consisting of a BREAK followed by a MARK AFTER BREAK and a START code. Valid data values under a NULL START code are between 0 and 255.  

The data transmission format is compounded as follows:

* Bit 1 start bit, low or space
* Bit 2-9 data bits (least significant bit to most significant bit)
* Bit 10,11 stop bits, high or MARK

The data frame is compounded by these time slots:

* BREAK : indicates the start of a new packet, typical value is 176 μsec.
* MARK AFTER BREAK : separates the break and start code time slots. Values can be between 8 μsec and 1 second.
* START CODE : the first slot (slot 0) following a MARK AFTER BREAK. Identifies the function of subsequent data bytes in the packet. For dimming commands the star code value is 0x00; therefore, it is also known as a NULL START CODE. Alternate start codes may be used. 
* DATA SLOTS : subsequent data bytes where the dimming levels for each receiving device are placed. The number of data slots is 512; DMX512 data packets with fewer data packets should not be transmitted. Time between any two data packets may vary from 0 up to 1 sec. This time is known as MARK TIME BETWEEN DATA SLOTS.
* MARK BEFORE BREAK : time between the second stop bit of the last data slot of a given data packet and the falling edge of the beginning of the BREAK for the next data packet. This time may vary from 0 up to 1 sec. Every data packet transmitted over the data link starts with a BREAK, MARK AFTER BREAK, and a START CODE sequence defined as a RESET sequence.
* BREAK-TO-BREAK : the period between the falling edge at the start code of any one BREAK and the falling edge at the start of the next BREAK. May vary from 1196 μsec up to 1.25 sec.

![DMX512 protocol figure](https://gitlab.com/accpnt/dmx/raw/master/modules/u_dmx512/doc/dmx512.svg)

Each listed point describes the numbers shown in Figure 1

1. SPACE for BREAK
2. MARK AFTER BREAK
3. Slot Time
4. START Time
5. LEAST SIGNIFICANT data bit
6. MOST SIGNIFICANT data bit
7. STOP bit
8. STOP bit
9. MARK TIME BETWEEN SLOTS
10. MARK BEFORE BREAK
11. BREAK to BREAK time
12. RESET sequence
13. DMX512 packet
14. START CODE (Slot 0, data)
15. SLOT 1, data
16. SLOT nnn, data (Max 512) 

## Design

The CPLD design is coded in VHDL. A proof of concept prototype has been designed for the DE0-NANO evaluation board. 

![DMX design figure](https://gitlab.com/accpnt/dmx/raw/master/modules/s_dmx/doc/dmx.svg)

## Todo

* Finish u_dmx512 interface module, with testbench
* Count the number of slots, return data byte and slot number as natural
* Handle number of slots in frame (there may be less than 512 slots in a frame)

## Links

[DMX512 Protocol Implementation Using MC9S08GT60 8-Bit MCU](https://www.nxp.com/docs/en/application-note/AN3315.pdf) 

[DMX512 transceiver](https://opencores.org/projects/dmx512) Verilog IP on OpenCores. 

[DMX decoder - Altera CPLD ](http://www.madinventions.co.uk/prj-DMXCPLD.html)

[DMX512-A](https://wiki.openlighting.org/index.php/DMX512-A) from the Open Lighting Project wiki. 

[DMXControl
](https://github.com/Adraub/DMXControl) is a VHDL implementation of a DMX 512 Controller on a Xilink FPGA